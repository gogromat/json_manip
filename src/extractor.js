/*
 * - Extracts specific k,v pairs in object of a dataset (add/remove rules)
 * - or replaces the objects with particular value (extract/replace rule)
 *   (useful when need only particular pairs in an object)
 */
 
/*jslint indent: 4 */
/*jslint browser: true */
/*jslint devel: true */
/*jslint nomen: true */
/*jslint unparam: true */

// global _ /
(function (_) {

    "use strict";

    var root = this,
        Jason = root.Jason,
        Helper = root.Jason.Helper,
        Extractor;

    function extractPairs(extractor) {

        var self = extractor,
            result_object = "",
            objects = self.objects,
            extractedObjects = self.extractedObjects;

        self.checkRules();

        var addRules = self.getAddRules(),
            removeRules = self.getRemoveRules(),
            extractRules = self.getExtractRules(),
            replaceRules = self.getReplaceRules();

        // Replaces current object in collection with it's inner value
        function extract(o, i) {
            _.each(extractRules, function (rule) {
                var context = rule.context;
                _.each(o, function (v, k) {
                    if (Helper[rule.name].call(context, v, k, o, i, extractedObjects)) {
                        extractedObjects.push(v);
                    }
                });
            });
        }
        

        function replaceObj(o, i) {
            
            var replacedObject = {};
            
            _.each(replaceRules, function (rule) {
                var action = rule.action, context = rule.context;
                // Replace the whole object if result is true
                if (action === "replace") {
                    var result = Helper[rule.name].call(context, o, i, extractedObjects);
                    if (result) {
                        replacedObject = result;
                    }
                // Only replace Key/Value
                } else {
                    _.each(o, function (v, k) {
                        var result = Helper[rule.name].call(context, v, k, o, i, extractedObjects);
                        if (action === "replaceKey") {
                            if (result) {
                                replacedObject[result] = o[k];
                            } else replacedObject[k] = v;
                        } else if (action === "replaceValue") {
                            replacedObject[k] = (result || v);
                        }
                    });
                }
            });
            return replacedObject;
        }


        // Return same object with different/modified K,V pairs
        function modify(o, i) {

            var extractorObject = {};

            // Add key rules
            _.each(addRules, function (rule) {
                var context = rule.context;
                _.each(o, function (v, k) {
                    if (Helper[rule.name].call(context, v, k, o, i, extractorObject) && !_.isNull(extractorObject)) {
                        extractorObject[k] = v;
                    }
                });
            });

            // if no add rules were given, simply make object as the original
            if (_.isEmpty(addRules) && _.isEmpty(extractRules)) {
                extractorObject = o;
            }

            // Remove key rules
            _.each(removeRules, function (rule) {
                var context = rule.context;
                _.each(o, function (v, k) {
                    if (Helper[rule.name].call(context, v, k, o, i, extractorObject) && !_.isNull(extractorObject)) {
                        delete(extractorObject[k]);
                    }
                });
            });

            return extractorObject;
        }


        if (!_.isEmpty(extractRules)) {
            _.each(objects, function (o, i) {
                extract(o, i);
            });
        }

        if (!_.isEmpty(replaceRules)) {
            _.each(objects, function (o, i) {
                result_object = replaceObj(o, i);
                if (!_.isNull(result_object)) extractedObjects.push(result_object);
            });
        }

        if (!_.isEmpty(addRules) || !_.isEmpty(removeRules)) {
            _.each(objects, function (o, i) {
                result_object = modify(o, i);
                if (!_.isNull(result_object)) extractedObjects.push(result_object);
            });
        }

        return extractedObjects;
    }

    Extractor = function (name, rules, objects) {
        this.name = name;
        this.setRules(rules);
        this.setObjects(objects);
    };

    var possible_actions = ["add", "remove", "extract", "replace"];

    Extractor.prototype.setRules = function (rules) {
        var self = this;
        self.rules = rules;
        _.each(possible_actions, function (r) {
            self[r + "Rules"] = _.filter(self.rules, function (rule) { 
                var re = new RegExp("^" + r);
                return rule.action.match(re); 
            });
        });
        return self;
    };
    
    _.each(possible_actions, function (r) {
        Extractor.prototype["get" + r.charAt(0).toUpperCase() + r.slice(1) + "Rules"] = function () {
            return this[r + "Rules"];  
        };
    });

    Extractor.prototype.checkRules = function () {
        if (!_.isArray(this.rules)) throw new Error("Jason DataSet Extractor: No rules were provided");
    };

    Extractor.prototype.setObjects = function (objects) {
        this.originalData = this.originalData || objects;
        this.objects = objects || this.originalData;
        this.extractedObjects = [];
    };

    Extractor.prototype.resetObjects = function () {
        this.extractedObjects = [];
    };

    Extractor.prototype.extract = function () {
        return extractPairs(this);
    };

    _.extend(Jason.Dataset.prototype, {
        extract: function (rules) {
            var self = this;
            if (!self.extractor) {
                self.extractor = new Extractor(self.name, rules, self.getData());
            } else if (self.extractor && rules) {
                self.extractor.setRules(rules);
                self.extractor.setObjects(self.getData());
            }
            Jason[self.name].objects = self.extractor.extract();
            return Jason[self.name];
        },
        replace: function (rules) {
            return this.extract(rules);
        }
    });

}).call(this, _);