/*
 * Jason.js
 * http://github.com/gogromat/json_manip
 * @author Alexander Astafurov
 * @license MIT License
 * @version 0.1
 *
 * Notes:  making isObject because underscore's isObject returns true for arrays and functions
 * 'val == null'  is logically equivallent as 'val === null || val === undefined'
 * 'val == string', the val is always coersed to string
 */
 
/*jslint indent: 4 */
/*jslint browser: true */
/*jslint devel: true */
/*jslint nomen: true */
/*jslint unparam: true */

// global _ /

// Jason
(function (_) {

    "use strict";

    var root = this,
        prevJason = root.Jason,
        Dataset,
        JasonHelper = {},
        Jason;    
    
    Jason = function (name, objects, rules) {

        if (!_.isString(name)) {
            throw new Error("Jason: Wrong object name provided (should be a string)");
        }

        if (Jason[name]) {
            throw new Error("Jason: " + name + " Dataset is already set!");
        }
        
        if (!_.isArray((JasonHelper.isObject(objects) ? objects = [objects] : objects))) {
            throw new Error("Jason: Wrong object type provided (should be array or object)");
        }

        if (rules) {
            JasonHelper.setRules(rules);
        }

        Jason[name] = new Dataset(name, objects);
        return Jason[name];
    };
    
    JasonHelper.toType = function (obj) {
        return ({}).toString.call(obj).match(/\s([a-z|A-Z]+)/)[1].toLowerCase();
    };

    JasonHelper.isObject = function (obj) {
        return this.toType(obj) == "object";
    };

    JasonHelper.isNumeric = function (num) {
        return !isNaN(parseFloat(num)) && isFinite(num);
    };

    JasonHelper.setStringifier = function (stringifyFunc) {
        this.stringifier = stringifyFunc;
    };

    JasonHelper.setParser = function (parserFunc) {
        this.parser = parserFunc;
    };

    Jason.stringify = JasonHelper.stringify = function (objects) {
        return (JasonHelper.stringifier ? JasonHelper.stringifier(objects) : JSON.stringify(objects));
    };

    Jason.parse = JasonHelper.parse = function (json_string) {
        return (JasonHelper.parser ? JasonHelper.parser(json_string) : JSON.parse(json_string));
    };


    Jason.setRule = JasonHelper.setRule = function (name, rule) {
        if (Jason[name] || JasonHelper[name]) {
            throw new Error("Jason Helper: Such rule already exists: " + name + ".");
        } else if (!_.isFunction(rule)) {
            throw new Error("Jason Helper: Wrong rule type provided");
        }
        Jason[name] = Jason.Helper[name] = rule;
    };
    
    Jason.setRules = JasonHelper.setRules = function (rules) {
        if (!_.isArray(rules)) {
            throw new Error("Jason Helper: Wrong rule type provided");
        }
        _.each(rules, function (rule) {
            if (!JasonHelper.isObject(rule) || !rule.name || !rule.callback) {
                throw new Error("Jason Helper: Wrong data type for rules provided");
            }
            JasonHelper.setRule(rule.name, rule.callback);
        });
    };
    
    // same as underscore's noConflict:
    // old Jason stays as "Jason", but assigns this namespace to the variable
    // that was assigned to noConflict call.
    Jason.noConflict = function () {
        root.Jason = prevJason;
        return this;
    };
    
    
    Dataset = function (name, objArr) {
        this.name = name;
        this.originalData = objArr;
        this.objects = null;
    };

    Dataset.prototype.getJsonData = function () {
        return JasonHelper.stringify(this.getData());
    };

    Dataset.prototype.getData = function () {
        var self = this;
        if (_.isEmpty(self.objects)) {
            self.objects = self.originalData;
        }
        return self.objects;
    };
    
    // alternative way to set the data
    Dataset.prototype.setData = function (callback, context) {
        this.objects = callback.call(context, this.objects);
        return this;
    };

    Dataset.prototype.getOriginalData = function () {
        return this.originalData;
    };

    Dataset.prototype.resetObjects = Dataset.prototype.resetData = function () {
        this.objects = '';
        return this;
    };

    Jason.Dataset = Dataset;
    root.Jason = Jason;
    root.Jason.Helper = JasonHelper;

}).call(this, _);