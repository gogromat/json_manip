/*
 * Experimental: can group objects (uses underscore's groupBy), 
 * and merge objects (those that were grouped by groupBy function)
 */

/*jslint indent: 4 */
/*jslint browser: true */
/*jslint devel: true */
/*jslint nomen: true */
/*jslint unparam: true */

// global _ /
(function (_) {

    "use strict";

    var root = this,
        Jason = root.Jason,
        Helper = root.Jason.Helper,
        Merger;


    function groupBy(merger, context) {
        var self = merger,
            objects = self.objects;
        self.checkRules();
        var mergeRules = self.getMergeRules(),
            mergeRule = mergeRules[0];
        self.mergedObjects = _.groupBy(objects, Helper[mergeRule.name], context);
        return self.mergedObjects;
    }


    function merge(merger) {
        var self = merger,
            mergedObjects = self.mergedObjects;
        //self.checkRules();
        var mergerObject = [];
        //mergeRules = self.getMergeRules(),

        // Groups
        _.each(mergedObjects, function (groupedBy, key) {
            var resultObject = {};
            // Objects in group, say [green, red, etc]
            _.each(groupedBy, function (object) {
                // Each key of an object
                _.each(object, function (value, key) {
                    if (_.isObject(value)) {
                        (_.has(resultObject, key) ? resultObject[key] : (resultObject[key] = [])).push(value);
                    } else {
                        resultObject[key] = value;
                    }
                });
            });
            mergerObject.push(resultObject);
        });
        _.each(mergerObject, function (value, key) {
            console.log(value, key);
        });
        return (self.mergedObjects = mergerObject);
    }



    Merger = function (name, rules, objects) {
        this.name = name;
        this.set_rules(rules);
        this.setObjects(objects);
        this.mergedObjects = [];
    };

    Merger.prototype.set_rules = function (rules) {
        this.rules = rules;
        this.mergeRules = _.filter(this.rules, function (o) {
            return o.action === "merge";
        });
    };

    Merger.prototype.getMergeRules = function () {
        return this.mergeRules;
    };

    Merger.prototype.checkRules = function () {
        if (!_.isArray(this.rules)) throw new Error("Jason DataSet Merger: No rules were provided");
    };


    Merger.prototype.setObjects = function (objects) {
        this.originalData = this.originalData || objects;
        this.objects = objects || this.originalData;
        this.mergedObjects = [];
    };

    Merger.prototype.resetObjects = function () {
        this.mergedObjects = [];
    };


    Merger.prototype.merge = function () {
        this.groupBy(this);
        return merge(this);
    };

    Merger.prototype.groupBy = function (context) {
        return groupBy(this, context);
    };

    _.extend(Jason.Dataset.prototype, {
        merge: function (rules) {
            var self = this;
            if (!self.merger) {
                self.merger = new Merger(self.name, rules, self.getData());
            } else if (self.merger && rules) {
                self.merger.set_rules(rules);
                self.merger.resetObjects(self.objects);
            }
            Jason[self.name].objects = self.merger.merge();

            return Jason[self.name];
        },
        groupBy: function (rules, context) {
            var self = this;
            if (!self.merger) {
                self.merger = new Merger(self.name, rules, self.getData());
            } else if (self.merger && rules) {
                self.merger.set_rules(rules);
                self.merger.resetObjects(self.objects);
            }
            Jason[self.name].objects = self.merger.groupBy(context);

            return Jason[self.name];
        }
    });

}).call(this, _);