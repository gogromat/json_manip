/*
 * Filters specific objects out of a dataset given some rules
 */

/*jslint indent: 4 */
/*jslint browser: true */
/*jslint devel: true */
/*jslint nomen: true */
/*jslint unparam: true */

// global _ /

(function (_) {

    "use strict";

    var root = this,
        Jason = root.Jason,
        Helper = root.Jason.Helper,
        Filtrator;

    function filterObjects(filterer) {

        var self = filterer;
        
        self.checkRules();
        
        self.filteredObjects = self.objects;
        
        _.each(self.rules, function (rule) {

            self.filteredObjects = (_.filter(self.filteredObjects, function (o, i, c) {
                var result = Helper[rule.name].call(rule.context, o, i, c);
                if (rule.action === "add") {
                    return (result ? true : false);
                } else if (rule.action === "remove") {
                    return (result ? false : true);
                }
                return false;
            }));

        });
        
        return self.filteredObjects;    
    }

    Filtrator = function (name, rules, objects) {
        this.name = name;
        this.setRules(rules);
        this.objects = objects;
        this.filteredObjects = [];
    };

    Filtrator.prototype.setRules = function (rules) {
        this.rules = rules;
    };
    
    Filtrator.prototype.checkRules = function () {
        if (!_.isArray(this.rules)) throw new Error("Jason DataSet Filtrator: No rules were provided");
    };
  
    Filtrator.prototype.setObjects = function (objects) {
        this.originalData = this.originalData || objects;
        this.objects = objects || this.originalData;
        this.filteredObjects = [];
    };

    Filtrator.prototype.resetObjects = function () {
        this.filteredObjects = [];
    };
    
    Filtrator.prototype.filter = function () {
        return filterObjects(this);
    };

    _.extend(Jason.Dataset.prototype, {
        filter: function (rules) {
            if (!this.filtrator) {
                this.filtrator = new Filtrator(this.name, rules, this.getData());
            } else if (this.filtrator && rules) {
                this.filtrator.setRules(rules);
            }
            this.objects = this.filtrator.filter();
            return Jason[this.name];
        },
        filterObjects: function (rules) {
            return this.filter(rules);
        }
    });

}).call(this, _);