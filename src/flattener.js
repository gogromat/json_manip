/*
 * Flattens JSON/Javascript objects and arrays in a dataset given some rules
 */
 
/*jslint indent: 4 */
/*jslint browser: true */
/*jslint devel: true */
/*jslint nomen: true */
/*jslint unparam: true */

// global _ /
(function (_) {

    "use strict";

    var root = this,
        Jason = root.Jason,
        Helper = root.Jason.Helper,
        Flattener;

    function flattenObjects(flattener) {

        var self = flattener,
            array_rule = self.getArrayRule(),
            object_rule = self.getObjectRule();

        self.resetObjects();

        //cache steps
        var isArrayStep = !_.isEmpty(array_rule.step) && _.isNumber(array_rule.step),
            isObjectStep = !_.isEmpty(object_rule.step) && _.isNumber(object_rule.step),
            arrayStep = parseInt(array_rule.step, 10), 
            objectStep = parseInt(object_rule.step, 10),
            arrayFlattening = array_rule.flattening_rule,
            objectFlattening = object_rule.flattening_rule;

        function flatten(object, result, step) {

            if (_.isUndefined(result)) {
                result = {}, step = 0;
            }
            step += 1;

            _.each(object, function (v, k) {

                if (_.isArray(v)) {

                    if (array_rule.action === "add") return result[k] = v;
                    else if (array_rule.action === "flatten") {
                        if (isArrayStep) {
                            if (step <= arrayStep) {
                                return flatten_inner_value(k, v, step, arrayFlattening, result);
                            }
                            return result[k] = v;
                        } else return flatten_inner_value(k, v, step, arrayFlattening, result);
                    }
                    
                } else if (_.isObject(v)) {

                    if (object_rule.action === "add") return result[k] = v;
                    else if (object_rule.action === "flatten") {
                        if (isObjectStep) {
                            if (step <= objectStep) {
                                return flatten_inner_value(k, v, step, objectFlattening, result);
                            }
                            return result[k] = v;
                        } else return flatten_inner_value(k, v, step, objectFlattening, result);
                    }
                    
                } else return result[k] = v;
            });
            return result;
        }

        function flatten_inner_value(k, v, step, rule, result) {
            _.each(flatten(v, {}, step), function (iv, ik) {
                result[k + rule + ik] = iv;
            });
        }

        // iterate over array of objects
        _.each(self.objects, function (object, index) {
            self.flattenedObjects.push(flatten(object));
        });

        return self.flattenedObjects;
    }


    function unflattenObjects(flattener, result) {

        var self = flattener,
        array_rule = self.getUnflattenArrayRule(),
        object_rule = self.getUnflattenObjectRule();
        
        function unflatten(object) {

            var resultObject = {};

            _.each(object, function (v, k) {
                // get array of keys
                var keys = splitPropertiesByKeys(k, array_rule.flattening_rule, object_rule.flattening_rule);
                // set the objects
                setObjectKeys(resultObject, keys, v);
            });
            return resultObject;
        }

        // iterate over array of objects
        _.each(self.objects, function (object, index) {
            self.flattenedObjects.push(unflatten(object));
        });

        return self.flattenedObjects;
    }

    // split flattened key string into array of keys
    function splitPropertiesByKeys(string, arr_rule, obj_rule) {
        return _.map(string.split(new RegExp("\\" + arr_rule + "|\\" + obj_rule)), function (v, k) {
            return (Helper.isNumeric(v) ? parseInt(v, 10) : v);
        });
    }

    // given array of keys, generate object keys as arrays/objects
    function setObjectKeys(object, keys, value) {

        var key = keys[0],
            rest_of_keys = keys.slice(1),
            inner_object;

        // last element
        if (_.isEmpty(rest_of_keys)) {
        
            object[key] = value;        

        // ... else more keys to go
        //array of objects
        } else if (_.isNumber(key)) {

            inner_object = {};

            if (!_.has(object, key))  object[key] = {};

            setObjectKeys(inner_object, rest_of_keys, value);

            _.each(inner_object, function (v, k) { this[k] = v; }, object[key]);

        //object
        } else {

            inner_object = (_.isNumber(rest_of_keys[0]) ? [] : {});

            setObjectKeys(inner_object, rest_of_keys, value);

            if (!_.has(object, key)) {
                object[key] = inner_object;
            } else {
                _.each(inner_object, function (v, k) {
                    (object[key][k] ? _.extend(object[key][k], v) : object[key][k] = v);
                });
            }
        }
        return object;
    }

    Flattener = function (name, rules, objects) {
        this.name = name;
        this.setObjects(objects);
        this.setRules(rules);
    };

    //todo: DRY up the functions
    Flattener.prototype.setRules = function (rules) {
        var self = this;
        self.rules = [];
        _.each(rules, function (rule) {
            rule.flattening_rule = rule.flattening_rule || ".";
            if (!_.contains(["isArray", "isObject"], rule.name)) {
                throw new Error("Jason: Flattener: Wrong rule provided");
            }
            self.rules.push(rule);
        });
    };
    
    Flattener.prototype.getArrayRule = function () {
        var self = this, provided = _.filter(self.rules, function (o) { return (o.name === "isArray" && o.action != "unflatten"); });
        return (!_.isEmpty(provided) ? provided[0] : {name: "isArray", action: "flatten", flattening_rule: "."});
    };
    
    Flattener.prototype.getObjectRule = function () {
        var self = this, provided = _.filter(self.rules, function (o) { return (o.name === "isObject" && o.action != "unflatten"); });
        return (!_.isEmpty(provided) ? provided[0] : {name: "isObject", action: "flatten", flattening_rule: "."});
    };
    
    Flattener.prototype.getUnflattenArrayRule = function () {
        var self = this, provided = _.filter(self.rules, function (o) { return (o.name === "isArray" && o.action == "unflatten"); });
        return (!_.isEmpty(provided) ? provided[0] : {name: "isArray", action: "unflatten", flattening_rule: "."});
    };
    
    Flattener.prototype.getUnflattenObjectRule = function () {
        var self = this, provided = _.filter(self.rules, function (o) { return (o.name === "isObject" && o.action == "unflatten"); });
        return (!_.isEmpty(provided) ? provided[0] : {name: "isObject", action: "unflatten", flattening_rule: "."});
    };
    
    Flattener.prototype.setObjects = function (objects) {
        this.originalData = this.originalData || objects;
        this.objects = objects || this.originalData;
        this.flattenedObjects = [];
    };

    Flattener.prototype.resetObjects = function () {
        this.flattenedObjects = [];
    };


    Flattener.prototype.flatten = function () {
        return flattenObjects(this);
    };

    Flattener.prototype.unflatten = function () {
        return unflattenObjects(this);
    };

    _.extend(Jason.Dataset.prototype, {
        flatten: function (rules) {
            if (!this.flattener) {
                this.flattener = new Flattener(this.name, rules, this.getData());
            } else {
                if (rules) this.flattener.setRules(rules);
                this.flattener.setObjects(this.getData());
            }
            this.objects = this.flattener.flatten();
            return this;
        },
        unflatten: function (rules) {
            if (!this.flattener) {
                this.flattener = new Flattener(this.name, rules, this.getData());
            } else { 
                if (rules) this.flattener.setRules(rules);
                this.flattener.setObjects(this.getData());
            }
            this.objects = this.flattener.unflatten();
            return this;
        }
    });

}).call(this, _);