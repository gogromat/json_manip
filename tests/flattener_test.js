describe('Flattener', function () {

  var JasonHelper = Jason.Helper;
  var sample_data = [
    {
        name: "Alexander",
        nickname: "GogromaT",
        studies: ["Com Sci", "Japanese", "Photography"],
        body: {
          gender: "Male", 
          height: "6.4\"", 
          eyes: "Blue"
        }
    },
    {
        name: "Constantine",
        nickname: "KA",
        likes: ["TED Talks", "Beer", "Movies", "Cars"],
        owns: [
            { 
              name: "car", 
              model: "Dodge Intrepid", 
              model_year: 2004
            }, 
            { 
              name: "camera", 
              model: "Pentax X-5"
            }
        ]
    }
  ];

  describe('Sample data', function () {

    var sample_data_1_result = Jason("sampleFl", sample_data).flatten().getData();

    it('flattening with no rules should equal flattened data', function () {
      expect(sample_data_1_result).toEqual(
        [ 
          { 
            "name" : 'Alexander', 
            "nickname" : 'GogromaT', 
            "studies.0" : 'Com Sci', "studies.1" : 'Japanese', "studies.2" : 'Photography', 
            "body.gender" : 'Male', "body.height" : "6.4\"", "body.eyes" : 'Blue'
          }, 
          { 
            "name" : "Constantine",
            "nickname" : "KA",
            "likes.0" : "TED Talks", "likes.1" : 'Beer', "likes.2" : 'Movies',"likes.3" : 'Cars', 
            "owns.0.name" : 'car',
            "owns.0.model" : 'Dodge Intrepid', "owns.0.model_year" : 2004, 
            "owns.1.name" : 'camera', "owns.1.model" : 'Pentax X-5' 
          } 
        ]);
    });

    Jason.sampleFl.resetData();

    // Arrays will be flattened with _ rule
    var sample_data_1_result_2 = Jason.sampleFl.flatten(
          [ 
            { name: "isArray", action: "flatten", flattening_rule: "_" },
            { name: "isObject", action: "flatten",flattening_rule: "." } 
          ]
        ).getData();

    it('flattening with array flattening rule: should equal flattened data', function () {
      expect(sample_data_1_result_2).toEqual([
        {
            "name": "Alexander",
            "nickname": "GogromaT",
            "studies_0": "Com Sci", "studies_1": "Japanese", "studies_2": "Photography",
            "body.gender": "Male", "body.height": "6.4\"", "body.eyes": "Blue"
        },
        {
            "name": "Constantine",
            "nickname": "KA",
            "likes_0": "TED Talks", "likes_1": "Beer", "likes_2": "Movies", "likes_3": "Cars",
            "owns_0.name": "car",
            "owns_0.model": "Dodge Intrepid", "owns_0.model_year": 2004,
            "owns_1.name": "camera", "owns_1.model": "Pentax X-5"
        }
      ]);
    });

    Jason.sampleFl.resetObjects();
    
    // Arrays will be removed, objects simply added (no flattening happens)
    var sample_data_1_result_3 = Jason.sampleFl.flatten(
        [ { name: "isArray",  action: "remove" },
          { name: "isObject", action: "add"    } ]
      ).getData();


    it('flattening with array flattening rule: should not have arrays but should have objects', function () {
      expect(sample_data_1_result_3).toEqual([
          {
            "name": "Alexander",
            "nickname": "GogromaT",
            "body": { "gender": "Male", "height": "6.4\"", "eyes": "Blue" }
          },
          { "name": "Constantine", "nickname": "KA" }
      ]);
    });

    Jason.sampleFl.resetObjects();

    // Adds arrays instead of flattening them
    var sample_data_1_result_4 = Jason.sampleFl.flatten(
        [ { name: "isArray", action: "add" },
          { name: "isObject", action: "flatten", flattening_rule: "." }]
        ).getData();

    it('flattening with array flattening rule: should add rules, flatten objects', function () {
      expect(sample_data_1_result_4).toEqual([
          {
              "name": "Alexander",
              "nickname": "GogromaT",
              "studies": ["Com Sci", "Japanese", "Photography"],
              "body.gender": "Male",
              "body.height": "6.4\"",
              "body.eyes": "Blue"
          },
          {
              "name": "Constantine",
              "nickname": "KA",
              "likes": [ "TED Talks","Beer", "Movies", "Cars" ],
              "owns": [ { "name": "car","model": "Dodge Intrepid","model_year": 2004 },
                        { "name": "camera","model": "Pentax X-5"} ]
          }
      ]);
    });


    var unflattened_data_4 = Jason.sampleFl.unflatten().getData();

    it('unflattening (no rules) (flat objects and normal arrays) should equal originalData', function () {
      expect(unflattened_data_4).toEqual(sample_data);
    });


  });


  describe('Unflattening', function () {

    Jason("unflattened", sample_data);
    var unflattened = Jason.unflattened.flatten().unflatten().getData();


    it("unflattener with no rules should return same data", function () {
      expect(unflattened).toEqual(sample_data);
    });

    describe('with errors', function () {

      Jason("unflattened2", sample_data);
      var flattening_rules = [
        {name: "isArray",  action: "flatten", flattening_rule: "+"},
        {name: "isObject", action: "flatten", flattening_rule: "$"}
      ];
      var flattened = Jason.unflattened2.flatten(flattening_rules).getData();

      it('flattened data should have same flattening rules', function () {
        expect( flattened[0]["body$gender"] ).toEqual("Male");
        expect( flattened[1]["likes+3"]     ).toEqual("Cars");
      });

      var unflattened_2 = Jason.unflattened2.unflatten().getData();

      it("unflattener with no rules and different flattened rule should fail", function () {
        expect(unflattened_2).not.toEqual(sample_data);
      });

    });


  });










  describe('Objects with Arrays', function () {

    var object = { 
      array: ["A", "simple", "array", "."], 
      some_other_data:":p" 
    };

    describe('with no rules for array', function () {

      var flattened_object_data = Jason('with_arrays', object).flatten().getData(),
          flattened_object      = flattened_object_data[0];

      it('should have array keys in object', function () {
        expect(flattened_object.array).toBeUndefined();
        expect(flattened_object["array.0"]).toBe("A");
        expect(flattened_object["array.1"]).toBe("simple");
        expect(flattened_object["array.2"]).toBe("array");
        expect(flattened_object["array.3"]).toBe(".");
        expect(flattened_object["array.4"]).toBeUndefined();
      });    
    });

    describe('with flattening rule for array', function () {

      var flattened_object_data = Jason.with_arrays.flatten(
        [{name:"isArray", action: "flatten"}]).getData(),
      flattened_object = flattened_object_data[0];

      it('should have array keys in object', function () {
        expect(flattened_object.array).toBeUndefined();
        expect(flattened_object["array.0"]).toBe("A");
        expect(flattened_object["array.1"]).toBe("simple");
        expect(flattened_object["array.2"]).toBe("array");
        expect(flattened_object["array.3"]).toBe(".");
        expect(flattened_object["array.4"]).toBeUndefined();
      });

      it("should not have same object's array as in initial object", function () {
        expect(flattened_object.array).not.toEqual(object.array);
      });

    });

    describe("with merge rule for array", function () {

      Jason.with_arrays.resetObjects();
      var flattened_object_data = Jason.with_arrays.flatten(
        [
          { name: "isArray", action: "flatten", flattening_rule: "+" }
        ]).getData(),
        flattened_object = flattened_object_data[0];

      it('should have different merging array rule in object', function () {
        expect(flattened_object["array"]).toBeUndefined();
        expect(flattened_object["array+0"]).toBe("A");
        expect(flattened_object["array+1"]).toBe("simple");
        expect(flattened_object["array+2"]).toBe("array");
        expect(flattened_object["array+3"]).toBe(".");
        expect(flattened_object["array+4"]).toBeUndefined();
      });

    });

  });





  describe('Flat Objects', function () {
  
    var flat_object, flattened_object, flattened_object_keys;

    flat_object = { i : 1, am : 4, a : "a", test : "Test", object : "" };

    flattened_object_data = Jason('test', flat_object).flatten().getData();
    flattened_object      = flattened_object_data[0];

    it('flattening should return same object for flat data', function () {
      expect(flattened_object).toEqual(flat_object);
    });

    it('unflattening should return same object for flat data', function () {
      unflattened_object_data = Jason.test.unflatten().getData();
      unflattened_object = unflattened_object_data[0];
      expect(unflattened_object).toEqual(flat_object);
    });

  });



});