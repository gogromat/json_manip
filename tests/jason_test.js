describe('Jason', function () {	

  describe('Exceptions', function () {

    var object = {a: 1},
        rule_1 = [{}],
        rule_2 = {},
        rule_3 = [
            { name : "test_1", callback : function () {} },
            { name : "test_2", callback : function () {} }
        ];
    
    //throw & expect some exceptions
    it('should throw Error on wrong name data type', function () {
      expect(function () { Jason(object, object); }).toThrow();
    });

    it('should throw Error on name that is already set', function () {
      Jason("exception", object);
      expect(function () { Jason("exception",object); }).toThrow();
    });

    it('should throw Error on wrong data type provided', function () {
      expect(function () { Jason("exception_1", "this is illegal"); }).toThrow();
    });

    it('should not throw Error on legal data type provided', function () {
      expect(function () { Jason("exception_1", object ); }).not.toThrow();
      expect(function () { Jason("exception_1.1", rule_1 ); }).not.toThrow();
    });

    it('should throw Error on wrong rules data type provided', function () {
      expect(function () { Jason("exception_2", object, "this is illegal"); }).toThrow();
      expect(function () { Jason("exception_2", object, rule_2);  }).toThrow();
    });

    it('should not throw Error on legal rule data type provided', function () {
      expect(function () { Jason("exception_2", object, rule_3 ); }).not.toThrow();
    });

  });


    describe("Dataset functions", function () {

        var data = [
            {     
                age:10, 
                name: {first:"Alex", last:"Test"}, 
                likes:["Tea","Coding"] 
            }
        ],
        modified_data = [{"name":"Some Modified Data"}],
        stringified_data = '[{"age":10,"name":{"first":"Alex","last":"Test"},"likes":["Tea","Coding"]}]';


        var new_data = Jason("newJason", data);

        it('should return stringified data', function () {
            expect(new_data.getJsonData()).toEqual( stringified_data );
        });

        it('should return original Data', function () {
            new_data.flatten();
            expect( new_data.getOriginalData() ).toEqual(data);
        });

        it('should modify data', function () {
            new_data.setData(function () { return modified_data; });
            expect( new_data.getData() ).toEqual(modified_data);
            expect( new_data.getData() ).toEqual(Jason.newJason.getData() );
        });


    });


});