describe('Filtrator & Extractor', function () {

	var sample_data = [
	    {
	        type: "Car",
	        age: 5,
	        location: "33.77651250680102, -118.20052746353142"
	    },
	    {
	        type: "Bus",
	        age: 15,
	        location: "33.77656601443673, -118.19382730541221"
	    },
	    {
	        type: "Airplain",
	        age: "14",
	        location: "34.91705225166044, -58.56598711894532"
	    },
	    {
	        type: "error",
	        some_property: "error"
	    }
	];

  describe('Sample data', function () {

    var sample_data_1_result = Jason("sample_filter", sample_data).extract(
    	[ 
    		{ name: "isNumeric", action: "add" } 
    	]).getData();

    it('should filter only numeric data', function () {
      expect(sample_data_1_result).toEqual(
      	[ { "age": 5  }, { "age": 15 }, { "age": "14"}, {}]
	  );
    });


	Jason.setRules([
	   {
	        name: "isLatLng",
	        callback: function (v, k) {
	            var re = /^-?\d+\.\d+,\s?-?\d+.\d+$/;
	            return re.test(v);
	        }
	    },
	    {
	        name: "isEmpty",
	        callback: function (o, i, c) { 
	            return _.isEmpty(o);
	        }
	    }
	]);

	Jason.sample_filter.resetObjects();

	var sample_data_1_result_2 = Jason.sample_filter.
		extract( [{ name: "isLatLng", action: "add"   }] ).
		filter(  [{ name: "isEmpty",  action: "remove"}] ).getData();

    it('should filter Lat/Lng data and remove empty objects', function () {
      expect(sample_data_1_result_2).toEqual(
      	[
		    {"location": "33.77651250680102, -118.20052746353142" },
		    {"location": "33.77656601443673, -118.19382730541221" },
		    {"location": "34.91705225166044, -58.56598711894532"  }
		]
	  );
    });

  });

});