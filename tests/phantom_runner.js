/*
 * PhantomJS runner for testem
 */

/*global phantom:false, require:false, console:false */

(function () {
	'use strict';

	//console.log('Loading localhost...');
	var page = require('webpage').create(),
		url = 'http://localhost:7357/';
	page.open(url, function (status) {
	    //Page is loaded!
	    //phantom.exit();
	});

}());