describe('Jason Helper', function() {

  var test_object_1 = {
    param: 'obj1'
  },  test_object_2 = {
    param: 'obj2'
  },  test_array_1 = [test_object_1, {}];

  it('should correctly check for object type', function () {
    expect( Jason.Helper.isObject(test_object_1) ).toBe(true);
  });

  it('should correctly check for wrong type (object)', function () {
    expect( Jason.Helper.isObject(test_array_1) ).toBe(false);
  });

  it('should correctly check for array type', function () {
    expect( _.isArray(test_array_1) ).toBe(true);
  });

  it('should correctly check for wrong type (array)', function () {
    expect( _.isArray(test_object_1) ).toBe(false);
  });

  it('should correctly output data types', function () {
    expect( Jason.Helper.toType ).toBeDefined();
    expect( Jason.Helper.toType([]) ).toEqual("array");
    expect( Jason.Helper.toType({}) ).toEqual("object");
    expect( Jason.Helper.toType(12) ).toEqual("number");
    expect( Jason.Helper.toType("") ).toEqual("string");
    expect( Jason.Helper.toType([]) ).toEqual(Jason.Helper.toType(test_array_1));
    expect( Jason.Helper.toType({}) ).toEqual(Jason.Helper.toType(test_object_2));
  });


  it('should only iterate over own properties of an object', function () {

    //object's own properties
    function newObj() {
      this.a = 1;
      this.b = 2;
      this.c = 3;
    };
    newObj.prototype = {
        d : 4, e : 5, f : 6
    }
    var newObjInstance = new newObj(),
        sum = 0;

    expect(sum).toBe(0);
    
    _.each(newObjInstance, function (value, key) { 
      sum += value; 
    }, sum);

    expect(sum).toBe(6);
  });


  describe('Exceptions', function () {

    var rule_1 = function () {},
        rule_2 = [],
        rules_3 = [
          { name:"rules_3_1", callback: function () {} },
          { name:"rules_3_2", callback: function () {} }
        ],
        rules_4 = [
          { name:"rules_3_3", callback: function () {} },
          { name:"rules_3_3", callback: function () {} }
        ];

    //throw & expect some exceptions
    it('should throw Error on wrong rule data type provided', function () {
      expect(function() { Jason.Helper.setRule("rule_2", rule_2); }).toThrow();
    });

    it('should not throw Error on legal rule data type provided', function () {
      expect(function () { Jason.Helper.setRule("rule_1", rule_1); }).not.toThrow();
      expect(function () { Jason.Helper.setRules(rules_3); }).not.toThrow();
    });

    it('should throw Error for the same rule name being set-up', function () {
      expect(function () { Jason.Helper.setRules(rules_4); }).toThrow();
    });
    
  });

});