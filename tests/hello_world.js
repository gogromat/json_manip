describe('Jasmine', function () {
  it('should say hello', function () {
    function hello() { return 'hello world'; }
    expect(hello()).toBe('hello world');
  });
});